resource "aws_security_group" "allow-ssh" {
  vpc_id = "${aws_vpc.gmatz.id}"
  name = "allow-ssh"
  description = "security group that allows ssh and all egress traffic"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  } 
tags {
    Name = "allow-ssh"
  }
}

resource "aws_security_group" "allow-postgres" {
  vpc_id = "${aws_vpc.gmatz.id}"
  name = "allow-postgres"
  description = "allow-postgres"
  ingress {
      from_port = 5432
      to_port = 5432
      protocol = "tcp"
      security_groups = ["${aws_security_group.allow-ssh.id}"] # allowing access from our airflow instance
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      self = true
  }
  tags {
    Name = "allow-postgres"
  }
}


resource "aws_security_group" "allow-airflow" {
  vpc_id = "${aws_vpc.gmatz.id}"
  name = "allow-airflow"
  description = "allow-airflow"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  } 
tags {
    Name = "allow-airflow"
  }
}
