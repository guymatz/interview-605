resource "aws_db_subnet_group" "incubator-subnet" {
    name = "incubator-subnet"
    description = "RDS subnet group"
    subnet_ids = ["${aws_subnet.gmatz-private-1.id}","${aws_subnet.gmatz-private-2.id}"]
}

resource "aws_db_parameter_group" "incubator-parameters" {
    name = "incubator-parameters"
    family = "postgres9.6"
    description = "incubator parameter group"

}


resource "aws_db_instance" "incubator" {
  allocated_storage    = 10
  engine               = "postgres"
  engine_version       = "9.6.5"
  instance_class       = "db.t2.micro"    # use micro if you want to use the free tier
  identifier           = "incubator"
  name                 = "incubator"
  username             = "root"
  password             = "incubator"
  db_subnet_group_name = "${aws_db_subnet_group.incubator-subnet.name}"
  parameter_group_name = "${aws_db_parameter_group.incubator-parameters.name}"
  multi_az             = "false"     # set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids = ["${aws_security_group.allow-postgres.id}"]
  storage_type         = "gp2"
  backup_retention_period = 30    # how long you’re going to keep your backups
  availability_zone = "${aws_subnet.gmatz-private-1.availability_zone}"   # prefered AZ
  skip_final_snapshot = true
  tags {
      Name = "incubator"
  }
}
