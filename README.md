# Airflow Installation

### TL;DR
To set up [Airflow](https://airflow.apache.org/index.html), make sure you have your [AWS credentials set up](https://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html) properly and:

  - git clone https://bitbucket.org/guymatz/interview-605.git
  - cd interview-605
  - terraform init (please tell me you have [terraform installed!](https://www.terraform.io/intro/getting-started/install.html))
  - terraform apply
  - Terraform will output an IP address.  Open up a web browser and go to http://IP_ADDRESS:8080/
  - Holla!!


### Implementation
This implementation cut many corners, for two reasons: First, one generally has supporting infrastructure in place, e.g. Jenkins, Ansible, Vault servers, that makes some things easier and other things possible.  Second, I chose not be concerned about some security aspects in order to avoid the later confusion of exchanging ansible vault passwords or credstash keys.  These two "conditions" led me to do some stupid things, such as hard-coding passwords.  It also precluded me from doing some other, more "sophisticated" practices, such as using an auto-scaling group which might have easier with Jenkins or Tower in place.

Other corners I cut:

  - Terraform provisioning
    - To make this project easier to "distribute" I left it as one repo.  I would normally put the ansible code in its own repo
    - I kept the terraform state in files rather than using S3 or Consul backend
    - I named the infrastructure poorly.  I would not normally use my own name to name a VPC, etc.
    - I didn't need all of the subnets I created.  I am a victim of my own copy/paste
    - I would normally set up monitoring/alerting for everything here
    - I would have used an SSL cert to force https and an ELB to front airflow to send 443 -> 8080
    - I would have used an EIP and DNS to make the site "permanent"
    - I would have looked into airflow to see what requirements it has so that I could make some better decision reqarding DB capacity, DB disk requirements
    - I would have committed more frequently, with better commit messages
    - I would have used [terragrunt](https://github.com/gruntwork-io/terragrunt) to make this code more modular and portable across the SDLC

- See ansible README for details on that Code . . .

### Time Spent
Muuuuuch more than I'd planned on!  I encountered issue after issue - since when is python only installed as /usr/bin/python3 and not /usr/bin/python ?!?!? - which caused much consternation/fretting/perturbation that either caused me to backtrack or move forward with regrets.  As Frank Sinatra once sang, "Moi, Je ne regrette rien!"  I guess I spent about 6 hours . . .

