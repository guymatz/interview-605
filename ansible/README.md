# Airflow Installation

### Implementation
Again, this code should be in its own repo.  Due to the choice of AMI in provisioning, weird things needed to be done to make this configuration management work . . .  the AMI did not include /usr/bin/python, so python3 needed to be specified for the ansible run.  This had cascading effects, including the need to specify /usr/bin/pip as the executable for the ansible pip module.

Other corners I cut:

  - Ansible
    - I would normally use a more appropriate DB user and an exncrypted password
    - I didn't make the DB initialization idempotent.  I would have checked the output in order to set changed_when and failed_when for that task
    - I would have written a README for the incubator role, itself
    - I would have cleaned up the role to remove unnecessary file
    - I would normally include an integration test at the end of a run
