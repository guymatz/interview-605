resource "aws_instance" "incubator" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.gmatz-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}",
                            "${aws_security_group.allow-airflow.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.mykeypair.key_name}"

  tags {
    Name = "incubator"
  }

}

resource "null_resource" "ssh_check" {

  provisioner "remote-exec" {
    inline = ["echo SSH is working."]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
      host        = "${aws_instance.incubator.public_ip}"
      agent       = "true"
    }
  }
}

resource "null_resource" "ansible" {

  # Why is python (2) not installed on this AMI!???!
  provisioner "local-exec" {
    command = <<EOF
    # Why is python (2) not installed on this AMI!???!
    ansible-playbook ansible/site.yml -e "db_string=${aws_db_instance.incubator.endpoint} ansible_python_interpreter=/usr/bin/python3" --user=ubuntu --private-key="${var.PATH_TO_PRIVATE_KEY}" -i ansible/inventory
  EOF
  }
  depends_on = ["null_resource.ssh_check"]
}

