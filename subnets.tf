# Subnets
resource "aws_subnet" "gmatz-public-1" {
    vpc_id = "${aws_vpc.gmatz.id}"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-2a"

    tags {
        Name = "gmatz-public-1"
    }
}
resource "aws_subnet" "gmatz-public-2" {
    vpc_id = "${aws_vpc.gmatz.id}"
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-2b"

    tags {
        Name = "gmatz-public-2"
    }
}
resource "aws_subnet" "gmatz-public-3" {
    vpc_id = "${aws_vpc.gmatz.id}"
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-2c"

    tags {
        Name = "gmatz-public-3"
    }
}
resource "aws_subnet" "gmatz-private-1" {
    vpc_id = "${aws_vpc.gmatz.id}"
    cidr_block = "10.0.4.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "us-east-2a"

    tags {
        Name = "gmatz-private-1"
    }
}
resource "aws_subnet" "gmatz-private-2" {
    vpc_id = "${aws_vpc.gmatz.id}"
    cidr_block = "10.0.5.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "us-east-2b"

    tags {
        Name = "gmatz-private-2"
    }
}
resource "aws_subnet" "gmatz-private-3" {
    vpc_id = "${aws_vpc.gmatz.id}"
    cidr_block = "10.0.6.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "us-east-2c"

    tags {
        Name = "gmatz-private-3"
    }
}
