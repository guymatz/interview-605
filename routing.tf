# Internet GW
resource "aws_internet_gateway" "gmatz-gw" {
    vpc_id = "${aws_vpc.gmatz.id}"

    tags {
        Name = "gmatz"
    }
}

# route tables
resource "aws_route_table" "gmatz-public" {
    vpc_id = "${aws_vpc.gmatz.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.gmatz-gw.id}"
    }

    tags {
        Name = "gmatz-public-1"
    }
}

# route associations public
resource "aws_route_table_association" "gmatz-public-1-a" {
    subnet_id = "${aws_subnet.gmatz-public-1.id}"
    route_table_id = "${aws_route_table.gmatz-public.id}"
}
resource "aws_route_table_association" "gmatz-public-2-a" {
    subnet_id = "${aws_subnet.gmatz-public-2.id}"
    route_table_id = "${aws_route_table.gmatz-public.id}"
}
resource "aws_route_table_association" "gmatz-public-3-a" {
    subnet_id = "${aws_subnet.gmatz-public-3.id}"
    route_table_id = "${aws_route_table.gmatz-public.id}"
}
